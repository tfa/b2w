const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Planeta = new Schema({
    nome:{
        type:String,
        required:true
    },
    clima: {
        type:String,
        required:true
    },
    terreno: {
        type:String,
        required:true
    },
    quantidade: {
        type:Number,
        required:true
    }
},
{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

mongoose.model("planeta", Planeta);