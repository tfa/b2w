const express = require('express');
const handlebars = require('express-handlebars');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express(); 
const admin = require('./routes/planetas');
const path = require('path');
const session = require('express-session');
const flash = require('connect-flash');
//Configurações

/*Session*/
app.use(session({
    secret:"aplicacaonode",
    resave:true,
    saveUninitialized:true
}));
app.use(flash());

/* Body parser */
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

/*Handlebars */
app.engine('handlebars',handlebars({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

/* Mongoose */
mongoose.connect('mongodb://localhost/planetas', {useNewUrlParser: true}).then(function(){
    console.log('CONECTADO AO MONGO');
}).catch(function(err){
    console.log('ERRO AO CONECTAR O BANCO DE DADOS');
});
/* Public */
app.use(express.static(path.join(__dirname,"public")));

/*Rota  ADMIN */
app.use('/', admin);


/*PORTA*/
const PORT = 8081;
app.listen(PORT, function(){
    console.log("Servidor rodando...");
});