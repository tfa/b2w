const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");
require("../models/Planeta");
const Planeta = mongoose.model("planeta");

/* LISTAR TODOS OS PLANETAS */

router.get('/planetas', function(req, res)
{
    Planeta.find().then(function(planetas){
        return res.status(201).send({planetas});
    }).catch(function(err){
        return res.status(400).send({error:'Não há planetas para listar'})
    });
});

/* EDITAR PLANETA */
router.post('/planetas/edit/:id', function(req, res)
{
    /* VERIFICA SE JA EXISTE */
    Planeta.findByIdAndUpdate({_id : req.body.id}).then(function(planeta){
       
        planeta.nome = req.body.nome;
        planeta.clima = req.body.clima;
        planeta.terreno = req.body.terreno;
        planeta.quantidade = req.body.quantidade;

        planeta.save().then(function(){
            return res.status(201).send({planeta});
        }).catch(function(err)
        {
            return res.satus(400).send({error:'Houve um erro interno ao salvar a edição de planeta'});
        });
        
    }).catch(function(err){
        return res.status(400).send({error:'Erro ao editar o planeta'});
    });
});

/* CADASTRAR PLANETAS */
router.post("/planetas/add", async function(req, res){

    const novoPlaneta = 
    {
        nome: req.body.nome, 
        clima: req.body.clima,
        terreno: req.body.terreno,
        quantidade: req.body.quantidade
    }

    /* INSERIR PLANETAS */
    new Planeta(novoPlaneta).save().then(function()
    {
        return res.status(201).send({novoPlaneta});     
    }).catch(function(err){
        return res.status(400).send({error:'Erro ao salvar planeta.'});
    });
});

/* DELETAR PLANETAS */
router.post("/planetas/delete", function(req, res)
{
    Planeta.deleteOne({_id:req.body.id}).then(function(){
        return res.status(201).send({sucesso:'Planeta deletado com sucesso'});
    }).catch(function(err){
        return res.status(400).send({error:'Houve um erro ao deletar o planeta'});
    });
});

/* BUSCAR PLANETAS */
router.post("/planetas/search", function(req, res, next)
{
    Planeta.find({nome:req.body.nome}).then(function(planetas){ 
        return res.status(200).send({planetas});
    }).catch(function(err){
        return res.status(400).send({error:'Este planeta não existe'});
    });
});  

/* BUSCAR POR ID PLANETAS */
router.post("/planetas/searchid", function(req, res, next)
{
    Planeta.findOne({_id:req.body.id}).then(function(planeta){
        return res.status(200).send({planeta});
    }).catch(function(err){
        return res.status(400).send({error:'Este ID planeta não existe'});
    });
});  
module.exports = router;